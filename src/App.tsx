import React, { FC, useState, useCallback, Component, useEffect } from 'react'
import { hot } from 'react-hot-loader/root'

const Counter: FC<{ counter: number; onChange: (counter: number) => any }> = ({
  counter: initialCounter = 0,
  onChange
}) => {
  const [counter, setCounter] = useState(initialCounter)
  const onPlusClick = useCallback(() => {
    const newCounter = counter + 1
    onChange(newCounter)
    setCounter(newCounter)
  }, [counter])
  const onMinusClick = useCallback(() => {
    const newCounter = counter - 1
    onChange(newCounter)
    setCounter(newCounter)
  }, [counter])
  const onResetClick = useCallback(() => {
    onChange(0)
    setCounter(0)
  }, [counter])

  return (
    <div>
      <h1>Counter: {counter}</h1>
      <button onClick={onPlusClick}>+</button>
      <button onClick={onMinusClick}>-</button>
      <button onClick={onResetClick}>reset</button>
    </div>
  )
}

const App: FC = () => {
  return (
    <div>
      <Counter counter={0} onChange={() => undefined}></Counter>
    </div>
  )
}

export default hot(App)
