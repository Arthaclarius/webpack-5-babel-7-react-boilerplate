import React, { FC } from 'react'

const Html: FC = ({ children }) => {
  return (
    <html>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <title>Webpack</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </head>
      <body>
        <div id="root-app">{children}</div>
        <script src="vendor-bundle.js"></script>
        <script src="main-bundle.js"></script>
      </body>
    </html>
  )
}

export default Html
