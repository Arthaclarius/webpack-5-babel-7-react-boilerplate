import express from 'express'
import path from 'path'
import webpack from 'webpack'
import expressStaticGzip from 'express-static-gzip'

import reactMiddleware from './middleware/reactMiddleware'
import {
  compilerMiddleware,
  hotMiddleware
} from './middleware/hotReloadMiddleware'

const productionMode = process.env.NODE_ENV === 'production'

const server = express()

server.use(expressStaticGzip(path.resolve('dist'), { enableBrotli: true }))

if (!productionMode) {
  const compiler = webpack(require('../config/webpack.dev'))
  server.use(compilerMiddleware(compiler))
  server.use(hotMiddleware(compiler))
}

server.use('/*', reactMiddleware).listen(8080, err => {
  if (err) process.exit(0)

  console.log('Server running at http://localhost:8080/')
})
