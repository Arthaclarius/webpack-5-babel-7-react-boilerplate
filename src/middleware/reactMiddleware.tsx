import React from 'react'
import { Handler } from 'express'
import { renderToStaticMarkup } from 'react-dom/server'
import App from '../App'
import Html from '../Html'

const reactMiddleware: Handler = (_req, res) => {
  res.send(`
  <!DOCTYPE html>
  ${renderToStaticMarkup(
    <Html>
      <App></App>
    </Html>
  )}
  `)
  res.status(200)
}

export default reactMiddleware
