import path from 'path'
import WebpackDevMiddleware from 'webpack-dev-middleware'
import WebpackHotMiddleware from 'webpack-hot-middleware'
import { Compiler } from 'webpack'

export const compilerMiddleware = (compiler: Compiler) =>
  WebpackDevMiddleware(compiler, {
    stats: {
      colors: true
    },
    ...({
      contentBase: path.resolve(__dirname, '../dist'),
      compress: true,
      overlay: true,
      hot: true,
      host: 'localhost'
    } as any)
  })
export const hotMiddleware = (compiler: Compiler) =>
  WebpackHotMiddleware(compiler)
